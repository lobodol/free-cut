# Installation
## Requirements
* Docker (1.12+)
* docker-compose (1.10+)
* GNU make

## Steps
1. Clone the project locally:

```bash
git clone git@gitlab.com:lobodol/free-cut.git
cd free-cut
```

2. Run command:

```bash
make start
```

3. Add the following line in your local `/etc/hosts` file:

```
127.0.0.1 freecut.dev
```

Then, go to [http://freecut.dev](http://freecut.dev/).